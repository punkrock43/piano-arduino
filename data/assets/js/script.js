const keys = document.querySelectorAll(".key"),
    note = document.querySelector(".nowplaying"),
    hints = document.querySelectorAll(".hints");

let isPlaying = false;
let currentRequest = null;

function playNoteKeyboard(e) {
    const key = document.querySelector(`.key[data-key="${e.keyCode}"]`);
    if (!key) return;
    playNote(key);
}

function playNoteMouse(e) {
    const key = e.target.closest(".key");
    if (!key) return;
    playNote(key);
}

async function playNote(key) {
    const keyNote = key.getAttribute("data-note");
    const keyValue = key.getAttribute("data-value");

    if (currentRequest) {
        currentRequest.abort();
    }

    key.classList.add("playing");
    note.innerHTML = keyNote;

    try {
        currentRequest = new AbortController();
        const response = await fetch(`/play?note=${keyValue}`, { signal: currentRequest.signal });
        if (response.ok) {
            const responseBody = await response.text();
            console.log(responseBody);
        } else {
            console.error(`Error: ${response.status} - ${response.statusText}`);
        }
    } catch (error) {
        if (error.name !== 'AbortError') {
            console.error("Network error:", error);
        }
    } finally {
        key.classList.remove("playing");
    }
}

function removePlayingClass() {
    this.classList.remove("playing");
}

keys.forEach((key) => key.addEventListener("transitionend", removePlayingClass));

window.addEventListener("keydown", playNoteKeyboard);
window.addEventListener("click", playNoteMouse);
