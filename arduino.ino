#include "config.h"
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <FS.h>

// Buzzer pin
const int buzzer = D2;

ESP8266WiFiMulti wifi_multi;
uint16_t connectTimeOutPerAP=5000;

ESP8266WebServer server(80);

void connectToWiFi()
{
    Serial.println("Connecting to the WiFi");
    wifi_multi.addAP(ssid1,password1);
    wifi_multi.addAP(ssid2,password2);
    
    Serial.println("Waiting for connection");
    while (wifi_multi.run(connectTimeOutPerAP)!=WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void setupServer()
{
    server.on("/play", HTTP_GET, []() {
        if (server.hasArg("note")) {
            String keyValueStr = server.arg("note");
            Serial.print("Parameter received: ");
            Serial.println(keyValueStr);

            // Convert the received string to an unsigned integer
            unsigned int frequency = keyValueStr.toInt();

            // Pass the frequency to the tone() function
            tone(buzzer, frequency, 100);

            server.send(200, "text/plain", "Note with frequency " + keyValueStr + " played");
        } else {
            server.send(400, "text/plain", "Bad Request: Note parameter missing");
        }
    });

    // Setup routes
    server.onNotFound([]() {                                  
        if (!handleFileRead(server.uri()))                    
            server.send(404, "text/plain", "404: Not Found"); 
    });

    // start the server
    server.begin();

    // print in Serial Monitor that the HTTP server is started
    Serial.println("HTTP server started");
}

void setup()
{
    Serial.begin(115200);
    delay(5000);
    connectToWiFi();

    // Initialize SPIFFS
    if (!SPIFFS.begin())
    {
        Serial.println("Failed to mount SPIFFS");
        return;
    }
    Serial.println("SPIFFS initialized successfully");

    Serial.println("Setting up server");
    setupServer();

    Serial.println("Setting buzzer to output");
    pinMode(buzzer, OUTPUT);
}

void loop()
{
  server.handleClient();
}

String getContentType(String filename)
{
    if (filename.endsWith(".htm"))
        return "text/html";
    else if (filename.endsWith(".html"))
        return "text/html";
    else if (filename.endsWith(".css"))
        return "text/css";
    else if (filename.endsWith(".js"))
        return "application/javascript";
    else if (filename.endsWith(".png"))
        return "image/png";
    else if (filename.endsWith(".gif"))
        return "image/gif";
    else if (filename.endsWith(".jpg"))
        return "image/jpeg";
    else if (filename.endsWith(".ico"))
        return "image/x-icon";
    else if (filename.endsWith(".xml"))
        return "text/xml";
    else if (filename.endsWith(".pdf"))
        return "application/x-pdf";
    else if (filename.endsWith(".zip"))
        return "application/x-zip";
    else if (filename.endsWith(".gz"))
        return "application/x-gzip";
    return "text/plain";
}

bool handleFileRead(String path)
{ // send the right file to the client (if it exists)
    Serial.println("handleFileRead: " + path);
    
    if (path.endsWith("/"))
        path += "index.html";                  // If a folder is requested, send the index file
    String contentType = getContentType(path); // Get the MIME type
    String pathWithGz = path + ".gz";
    if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))
    {                                                       // If the file exists, either as a compressed archive, or normal
        if (SPIFFS.exists(pathWithGz))                      // If there's a compressed version available
            path += ".gz";                                  // Use the compressed version
        File file = SPIFFS.open(path, "r");                 // Open the file
        size_t sent = server.streamFile(file, contentType); // Send it to the client
        file.close();                                       // Close the file again
        Serial.println(String("\tSent file: ") + path);
        return true;
    }
    Serial.println(String("\tFile Not Found: ") + path);
    return false; // If the file doesn't exist, return false
}
